FactoryBot.define do
  factory :user do
    username { "MyString" }
    image { "MyString" }
    description { "MyString" }
    twitter { "MyString" }
    website { "MyString" }
    location { "MyString" }
  end
end
