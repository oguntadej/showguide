FactoryBot.define do
  factory :show do
    title { "MyString" }
    premiere_on { "2018-12-18" }
    ended_on { "2018-12-18" }
    official_site { "MyString" }
    imdb { "MyString" }
    network { "MyString" }
    country { "MyString" }
    status { "MyString" }
    runtime { "MyString" }
    days { "MyText" }
    medium_image { "MyString" }
    large_image { "MyString" }
    summary { "MyText" }
    genres { "MyText" }
    channel { "MyString" }
  end
end
