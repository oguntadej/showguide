class UsersController < ApplicationController
  before_action :authentication_required, except: [:callback]

  def callback
    user = User.save_user_data(user_info)
    session[:user] = user.id
    redirect_to root_url
  end

  def show
    @user = User.find_by(username: params[:username])
    @shows = Show.all
  end

  def profile
    @user = current_user
    render 'users/show'
  end

  def destroy
    session[:user] = nil
    redirect_to root_url
  end


  private

  def user_info
    request.env['omniauth.auth']['info']
  end
  
end
