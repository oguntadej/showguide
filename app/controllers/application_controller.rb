class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user

  def current_user
    @user ||= User.includes(:recommendations).find_by(id: session[:user])
  end

  def authentication_required
       redirect_to root_url, error: 'Please log in to continue' unless current_user
  end
end
