class ShowsController < ApplicationController
  before_action :authentication_required, only: [:recommend]
  
  def index
    @shows = Show.order(created_at: :desc).all
  end

  def show
    @show = Show.find_by(id: params[:id])
    redirect_to root_url if @show.nil?
  end

  def create
    query = params[:show]
    response = Tvmaze::GetShowData.new.call(query)
    Show.save_show(response)
    redirect_to root_url
  end

  def recommend
    Recommendation
        .where(show_id: params[:show], user_id: current_user.id)
        .find_or_create_by!(show_id: params[:show], user_id: current_user.id)
    redirect_to show_url(params[:show])
  end

  def search
    @shows = Show.where('title ILIKE ?', "%#{params[:query]}%")
    render 'shows/index'
  end
end
