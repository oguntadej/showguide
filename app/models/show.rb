class Show < ApplicationRecord
  has_many :recommendations, ->{order(created_at: :desc)}

  def self.save_show(data)
    show = Show.where(title: data['name']).first_or_initialize
    show.title = data['name']
    show.premiere_on = data['premiered']
    show.official_site = data['officialSite']
    show.imdb = data['externals']['imdb']
    show.network = data['network']['name']
    show.country = data['network']['country']['code']
    show.status = data['status']
    show.runtime = data['runtime']
    show.days = data['schedule']['days']
    show.medium_image = data['image']['medium']
    show.large_image = data['image']['original']
    show.summary = data['summary']
    show.genre = data['genres']
    show.save!
    show
  end
end
