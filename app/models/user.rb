class User < ApplicationRecord
  has_many :recommendations, ->{order(created_at: :desc)}

  def self.save_user_data(data)
    user = User.where(username: data.nickname).first_or_initialize
    user.username = data.nickname
    user.image = data.image
    user.description = data.description
    user.twitter = data.urls['Twitter']
    user.website = data.urls['Website']
    user.location = data.location
    user.save!
    user
  end
end
