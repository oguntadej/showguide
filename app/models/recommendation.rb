class Recommendation < ApplicationRecord
  belongs_to :user, counter_cache: true
  belongs_to :show, counter_cache: true
end
