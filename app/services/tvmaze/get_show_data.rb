module Tvmaze
  class GetShowData
    def initialize
      @connection = Faraday.new(url: 'http://api.tvmaze.com')
    end

    def call(show)
      get_show_data(show)
    end

    def get_show_data(show)
      query = URI.encode(show)
      response = @connection.get "/singlesearch/shows?q=#{query}"
      JSON.parse(response.body)
    end
  end
end