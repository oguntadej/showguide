module ShowsHelper
  def show_recommended?(show_id)
    Recommendation.where(show_id: show_id).empty?
  end
end
