Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'shows#index'
  get '/search', to: 'shows#search', as: 'search'

  get '/show/:id', to: 'shows#show', as: 'show'
  get '/add', to: 'shows#create', as: 'add_show'
  get 'auth/:provider/callback', to: 'users#callback', as: 'callback'
  get '/user/:username', to: 'users#show', as: 'user'
  delete '/logout', to: 'users#destroy', as: 'logout'


  get 'profile', to: 'users#profile', as: 'profile'

  post '/recommend/:show', to: 'shows#recommend', as: 'recommend'
end
