# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_19_214343) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "recommendations", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "show_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["show_id"], name: "index_recommendations_on_show_id"
    t.index ["user_id"], name: "index_recommendations_on_user_id"
  end

  create_table "shows", force: :cascade do |t|
    t.string "title", limit: 240, null: false
    t.date "premiere_on"
    t.string "official_site", limit: 200
    t.string "imdb", limit: 20
    t.string "network", limit: 240, null: false
    t.string "country", limit: 4, null: false
    t.string "status", limit: 20
    t.string "runtime", limit: 4
    t.text "days", default: [], array: true
    t.string "medium_image", limit: 240, null: false
    t.string "large_image", limit: 240, null: false
    t.text "summary"
    t.text "genre", default: [], array: true
    t.integer "recommendations_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["country"], name: "index_shows_on_country"
    t.index ["network"], name: "index_shows_on_network"
    t.index ["status"], name: "index_shows_on_status"
    t.index ["title"], name: "index_shows_on_title"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", limit: 140, null: false
    t.string "image"
    t.string "description", limit: 400
    t.string "twitter"
    t.string "website"
    t.string "location"
    t.integer "recommendations_count", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["image"], name: "index_users_on_image"
    t.index ["username"], name: "index_users_on_username"
  end

  add_foreign_key "recommendations", "shows"
  add_foreign_key "recommendations", "users"
end
