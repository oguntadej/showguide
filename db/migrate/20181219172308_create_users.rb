class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, null: false, unique: true, index: true, limit: 140
      t.string :image, index: true
      t.string :description, limit: 400
      t.string :twitter, unique: true
      t.string :website
      t.string :location
      t.integer :recommendations_count, default: 0

      t.timestamps
    end
  end
end
