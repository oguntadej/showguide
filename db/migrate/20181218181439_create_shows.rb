class CreateShows < ActiveRecord::Migration[5.2]
  def change
    create_table :shows do |t|
      t.string :title, limit: 240, null: false, index: true, unique: true
      t.date :premiere_on
      t.string :official_site, limit: 200
      t.string :imdb, limit: 20
      t.string :network, null: false, index: true, limit: 240
      t.string :country, null: false, index: true, limit: 4
      t.string :status, index: true, limit: 20
      t.string :runtime, limit: 4
      t.text :days, array: true, default: []
      t.string :medium_image, null: false, limit: 240
      t.string :large_image, null: false, limit: 240
      t.text :summary
      t.text :genre, array: true, default: []
      t.integer :recommendations_count, default: 0
      t.timestamps
    end
  end
end
